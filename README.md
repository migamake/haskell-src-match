# Goal

Leverage your knowledge of haskell-src-exts for parsing Haskell source files, and checking that they contain prescribed fragments.
This is a general problem of functional testing of code generators:
One would like to make it modular, but at the same time as close to produced source as possible.
I propose that you parse a Haskell output from code generator with haskell-src-exts and then match on the produced file.
Matching would be best done using TemplateHaskell.
Or you may also use Haskell-src-exts here in quasiquotes.
so this could be an independent library that we use to make xml-typelift and json-autotype better.

# Milestones

1. Parse input module with `haskell-src-exts`, and get a list of type declarations, and a list of instance declarations.
  (Easy)
2. Use `haskell-src-exts` to get a data structure from quasiquote.
3. Make a matching function out of a data structure returned by quasiquote.
4. Add convenience interface to parse output file with `haskell-src-exts` and match patterns.
5. Change all identifiers starting with an underbar (like `_function`) to be variables.
   Modify matching function accordingly
6. Change all type identifiers starting with an `U_` to be variables.
7. Change all class identitifers starting with an `U_` to be variables.
8. Make a test suite for xml-typelift using this.

# Possible improvements for the future

Not yet clear that we need this last feature for testing:
* Generate variable substitutions from successful match.
