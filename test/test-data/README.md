Example test for `json-autotype`:
1. Input file for `json-autotype` - `colors.json`
2. Output file from `json-autotype` - `colors.hs`
3. Test that reads output file and checks that declaration exists: `TestSourceMatch.hs`.
